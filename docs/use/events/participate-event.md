# Participate in an event

## With an account

When you want to participate in an event:

  1. display the event page (it's easier this way ;))
  * click the **Participate** button below the event banner
  * select which profile you want to use for this event

![event participation with account img](../../images/en/event-tree-participation-rose-utopia-EN.jpg)

That's it! The **Participate** button is now **I participate**.

!!! note
    If you click the **Participate** button without being logged in, you will be prompted to do so.

!!! info
    If participation approval is activated, you will have to wait until an organizer approves your participation.

## Anonymously

!!! note
    Organizer has to **allow** [anonymous participation](../../events/create-events/#who-can-view-this-event-and-participate). By default, events do not allow for it.

To particpate in an event anonymously:

  1. display the event page (it's easier this way ;))
  * click the **Participate** button below the event banner
  * click **I don't have a Mobilizon account** to participate using your email address
  * enter your email address in the **Email** field
  * [Optional] write a message to the organizer(s)
  * click **Send email**

![anonymous participation img](../../images/en/rose-utopia-anonymous-participation-web-EN.png)

An email will be sent to you to confirm your email address. Click **Confirm my e-mail address**. Congratulations: your participation has been validated!

### Remember my participation in this browser

**Optional**: You can check **Remember my participation in this browser**: it will allow you to display and manage your participation status on the event page when using this device. **Please uncheck it** if you're using a public device.

If checked, you can cancel your anonymous participation by clicking the **Cancel anonymous participation** button below the event banner.

!!! info
    If participation approval is activated, you will have to wait until an organizer approves your participation.
