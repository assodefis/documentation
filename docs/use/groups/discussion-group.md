# Group discussions

 Keep the entire conversation about a specific topic together on a single page.

!!! note
    You have to be a moderator or administrator to start / edit / delete a discussion. See [roles](../../groups/roles-group/#roles).

## Start a new discussion

To start a new discussion in your group, you have to, on your group page:

  1. click the **+ Start a discussion** button in **Discussions** section
  * add a title
  * add some text
  * click the **Create the discussion** button

!!! note
    You also can start a new discussion by clicking **View all**
    ![image view all discussions link](../../images/en/group-discussions-view-all.png)

Your topic is now visible in the **Discussions** section. Everyone in the group can reply.

## Edit and delete a discussion

### Edit a discussion

To edit a title, you have to:

  1. open the discussion by clicking its title in the group list
  * click <svg style="width:24px;height:24px" viewBox="0 0 24 24">
    <path fill="currentColor" d="M20.71,7.04C21.1,6.65 21.1,6 20.71,5.63L18.37,3.29C18,2.9 17.35,2.9 16.96,3.29L15.12,5.12L18.87,8.87M3,17.25V21H6.75L17.81,9.93L14.06,6.18L3,17.25Z" />
    </svg> in front of the title
  * make changes
  * click <svg style="width:24px;height:24px" viewBox="0 0 24 24">
    <path fill="currentColor" d="M21,7L9,19L3.5,13.5L4.91,12.09L9,16.17L19.59,5.59L21,7Z" />
    </svg> below the title field  (or **x** to cancel your action)

![gif discussion title edition](../../images/en/group-discussion-edit.gif)

### Delete a discussion

To delete a discussion, you have to:

  1. open the discussion by clicking it's title in the group list
  * click <svg style="width:24px;height:24px" viewBox="0 0 24 24">
    <path fill="currentColor" d="M20.71,7.04C21.1,6.65 21.1,6 20.71,5.63L18.37,3.29C18,2.9 17.35,2.9 16.96,3.29L15.12,5.12L18.87,8.87M3,17.25V21H6.75L17.81,9.93L14.06,6.18L3,17.25Z" />
    </svg> in front of the title
  * click the <svg style="width:24px;height:24px" viewBox="0 0 24 24">
    <path fill="currentColor" d="M19,4H15.5L14.5,3H9.5L8.5,4H5V6H19M6,19A2,2 0 0,0 8,21H16A2,2 0 0,0 18,19V7H6V19Z" />
    </svg> **Delete conversation** button (or **x** to cancel your action)

![image discussion deleteion](../../images/en/group-discussion-delete.png)

!!! danger
    Every member of the group can edit and **delete** a discussion!

## Edit and delete a comment

### Edit a comment

To edit a comment you made, you have to:

  1. click **⋅⋅⋅** next to your comment date
  * click <svg style="width:24px;height:24px" viewBox="0 0 24 24">
    <path fill="currentColor" d="M20.71,7.04C21.1,6.65 21.1,6 20.71,5.63L18.37,3.29C18,2.9 17.35,2.9 16.96,3.29L15.12,5.12L18.87,8.87M3,17.25V21H6.75L17.81,9.93L14.06,6.18L3,17.25Z" />
    </svg> **Edit**
  * make changes
  * click the **Update** button below comment area

### Delete a comment

To delete a comment you made, you have to:

  1. click **⋅⋅⋅** next to your comment date
  * click the <svg style="width:24px;height:24px" viewBox="0 0 24 24">
    <path fill="currentColor" d="M19,4H15.5L14.5,3H9.5L8.5,4H5V6H19M6,19A2,2 0 0,0 8,21H16A2,2 0 0,0 18,19V7H6V19Z" />
    </svg> **Delete** button
