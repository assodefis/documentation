# CLI Tasks

Mobilizon provides commands to perform some tasks. Explore the tasks in the menu.

=== "Releases"

    The commands need to be called like this:
    ```bash
    sudo -u mobilizon ./bin/mobilizon_ctl command.subcommand
    ```

    For instance, creating a new admin would be something like:
    ```bash
    sudo -u mobilizon ./bin/mobilizon_ctl users.new "your@email.com" --admin --password "Y0urP4ssw0rd"
    ```

=== "Docker"

    The commands need to be called like this:
    ```bash
    mobilizon_ctl command.subcommand
    ```

    For instance, creating a new admin would be something like:
    ```bash
    docker-compose exec mobilizon mobilizon_ctl users.new "your@email.com" --admin --password "Y0urP4ssw0rd"
    ```

=== "Source"
    !!! tip "Environment"
        You need to run these commands with the appropriate environment loaded, so make sure they're alwayed prefixed with `MIX_ENV=prod`.

    The commands need to be called like this:
    ```bash
    MIX_ENV=prod mix mobilizon.command.subcommand
    ```

    For instance, creating a new admin would be something like:
    ```bash
    MIX_ENV=prod mix mobilizon.users.new "your@email.com" --admin --password "Y0urP4ssw0rd"
    ```
