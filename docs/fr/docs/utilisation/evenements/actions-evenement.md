# Partager, signaler, éditer un événement

Chaque événement possède un bouton **Actions**. Les options dépendent de vos permissions sur cet événement.

## Partager l'événement

!!! note
    Cette action est disponible pour tout le monde.

Pour partager un événement vous devez&nbsp;:

  1. aller sur sa page et cliquer sur le bouton **Actions <svg style="width:24px;height:24px" viewBox="0 0 24 24">
    <path fill="currentColor" d="M16,12A2,2 0 0,1 18,10A2,2 0 0,1 20,12A2,2 0 0,1 18,14A2,2 0 0,1 16,12M10,12A2,2 0 0,1 12,10A2,2 0 0,1 14,12A2,2 0 0,1 12,14A2,2 0 0,1 10,12M4,12A2,2 0 0,1 6,10A2,2 0 0,1 8,12A2,2 0 0,1 6,14A2,2 0 0,1 4,12Z" />
</svg>**
  * cliquer sur **Partager l'événement** <svg style="width:24px;height:24px" viewBox="0 0 24 24">
    <path fill="currentColor" d="M21,12L14,5V9C7,10 4,15 3,20C5.5,16.5 9,14.9 14,14.9V19L21,12Z" /></svg>
  * cliquer sur <svg style="width:24px;height:24px" viewBox="0 0 24 24">
    <path fill="currentColor" d="M19,20H5V4H7V7H17V4H19M12,2A1,1 0 0,1 13,3A1,1 0 0,1 12,4A1,1 0 0,1 11,3A1,1 0 0,1 12,2M19,2H14.82C14.4,0.84 13.3,0 12,0C10.7,0 9.6,0.84 9.18,2H5A2,2 0 0,0 3,4V20A2,2 0 0,0 5,22H19A2,2 0 0,0 21,20V4A2,2 0 0,0 19,2Z" />
</svg> pour copier le lien vers l'événement ou sur l'icône de la plateforme sur laquelle vous souhaitez le partager

![image de la modale de partage](../../images/event-share-FR.png)

## Ajouter à mon agenda

Cette fonctionnalité vous permet d'ajouter le fichier `.ics` à votre logiciel de calendrier (Thunderbird, par exemple).
Pour ce faire vous devez&nbsp;:

  1. cliquer sur **Ajouter à mon agenda** <svg style="width:24px;height:24px" viewBox="0 0 24 24">
    <path fill="currentColor" d="M19 19V8H5V19H19M16 1H18V3H19C20.11 3 21 3.9 21 5V19C21 20.11 20.11 21 19 21H5C3.89 21 3 20.1 3 19V5C3 3.89 3.89 3 5 3H6V1H8V3H16V1M11 9.5H13V12.5H16V14.5H13V17.5H11V14.5H8V12.5H11V9.5Z" />
    </svg>
  * cocher **Ouvrir avec** et choisir votre logiciel d'agenda
  * cliquer sur **OK**

![modale de partage ics](../../images/event-share-ics-EN.png)

## Signalement

!!! note
    Vous devez être connecté⋅e à votre compte Mobilizon pour faire une signalement.

Pour signaler un événement, vous devez cliquer sur **Signalement** <svg style="width:24px;height:24px" viewBox="0 0 24 24"><path fill="currentColor" d="M14.4,6L14,4H5V21H7V14H12.6L13,16H20V6H14.4Z" /></svg>

![modale de signalement d'un événement](../../images/event-report-modal-FR.png)

## Modifier

!!! note
    Vous devez être admin ou modérateur/modératrice pour voir ces options

Pour modifier, dupliquer un événement vous devez cliquer sur le bouton **Actions** puis&nbsp;:

  * **Modifier** (voir [Créer un événement](creation-evenement.md))
  * **Dupliquer** pour ouvrir une page remplie des mêmes paramètres que l'événement

## Supprimer

!!! note
    Vous devez être admin ou modérateur/modératrice pour voir ces options

Pour supprimer un événement vous devez&nbsp;:

  1. cliquer sur le bouton **Actions** de celui-ci
  * cliquer sur **Supprimer**
  * indiquer le titre de l'événement dans le champ
  * cliquer sur le bouton **Supprimer "le nom de votre événement"**

![modale de suppression d'un événement](../../images/delete-event-FR.png)
